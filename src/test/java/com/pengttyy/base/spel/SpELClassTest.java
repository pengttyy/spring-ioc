package com.pengttyy.base.spel;

import org.junit.Before;
import org.junit.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.*;

import static org.junit.Assert.*;

/**
 * 类类型表达式、类实例化、变量定义及引用、自定义函数
 * ，赋值表达式、对象属性存取及安全导航表达式、对象方法调用
 * Created by KaiPeng on 2016/7/7.
 */
public class SpELClassTest {
    private ExpressionParser expressionParser;
    private EvaluationContext context;

    @Before
    public void setUp() throws Exception {
        //解析器
        expressionParser = new SpelExpressionParser();
        //上下文
        context = new StandardEvaluationContext();
    }

    /**
     * T(类全限定名)，java.lang包除外
     *
     * @throws Exception
     */
    @Test
    public void class_type_el() throws Exception {
        Class aClass = simpleExpress("T(String)", Class.class);
        assertEquals(String.class, aClass);
        Class aClass1 = simpleExpress("T(com.pengttyy.base.spel.SpELClassTest)", Class.class);
        assertEquals(SpELClassTest.class, aClass1);
    }

    @Test
    public void static_field_access() throws Exception {
        Integer integer = simpleExpress("T(Integer).MAX_VALUE", int.class);
        assertEquals(Integer.MAX_VALUE, integer.intValue());

        Integer integer1 = simpleExpress("T(Integer).parseInt('1')", int.class);
        assertEquals(1, integer1.intValue());
    }

    /**
     * new 一个对象
     *
     * @throws Exception
     */
    @Test
    public void new_object() throws Exception {
        String express = simpleExpress("new String('hello World')", String.class);
        assertEquals("hello World", express);
    }

    @Test
    public void variable_defind() throws Exception {
        Date date = new Date();
        context.setVariable("papa", date);
        Date value = expressionParser.parseExpression("#papa").getValue(context, Date.class);
        assertEquals(date, value);

        Object value1 = expressionParser.parseExpression("#this").getValue(context, String.class);
        System.out.println(value1);
    }

    @Test
    public void method_defind() throws Exception {
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.registerFunction("toInt", Integer.class.getDeclaredMethod("parseInt", String.class));
        Integer value = expressionParser.parseExpression("#toInt('10')").getValue(context, int.class);
        assertEquals(10, value.intValue());
    }

    @Test
    public void propetry_arcess() throws Exception {
        context.setVariable("myDate", new Date());
        Integer value = expressionParser.parseExpression("#myDate.month").getValue(context, int.class);
        assertTrue(value > 0);
    }

    @Test
    public void safe_arcess() throws Exception {
        //安全访问操作符?.
        context.setVariable("myNull", null);
        Integer valueNull = expressionParser.parseExpression("#myNull?.month").getValue(context, Integer.class);
        assertNull(valueNull);
    }

    @Test
    public void instance_method_arcess() throws Exception {
        Date date = new Date();
        context.setVariable("myDate", date);
        Integer value = expressionParser.parseExpression("#myDate.getMonth()").getValue(context, int.class);
        assertEquals(date.getMonth(), value.intValue());
    }

    @Test
    public void list_express_ok() throws Exception {
        List list = simpleExpress("{}", List.class);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void list_express_unmodifiable() throws Exception {
        List list = simpleExpress("{}", List.class);
        list.add("xyz");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void list_express_unmodifiable2() throws Exception {
        List list = simpleExpress("{1,2,3}", List.class);
        list.add(4);
    }

    /**
     * 语法：（list|map）.![投影表达式]
     *
     * @throws Exception
     */
    @Test
    public void use_projection() throws Exception {
        List<Integer> list = getIntegers();
        context.setVariable("list", list);
        List result = simpleExpress("#list.![0/#this]", List.class);//#this代表当前集合中的每一个元素
        assertEquals(list.size(), result.size());
    }

    @Test
    public void use_projection_map() throws Exception {
        Map<Integer, String> map = getIntegerStringMap();
        context.setVariable("map", map);
        List list = simpleExpress("#map.![key+1]", List.class);
        assertEquals(map.size(), list.size());

        List list2 = simpleExpress("#map.![value+1]", List.class);
        assertEquals(map.size(), list2.size());
    }

    /**
     * 语法：(list|map).?[选择表达式]
     *
     * @throws Exception
     */
    @Test
    public void select_projection_list() throws Exception {
        List<Integer> list = getIntegers();
        context.setVariable("list", list);
        List projections = simpleExpress("#list.?[#this>2]", List.class);
        assertEquals(3, projections.size());
    }

    @Test
    public void select_projection_map() throws Exception {
        Map<Integer, String> map = getIntegerStringMap();
        context.setVariable("map", map);
        List list = simpleExpress("#map.?[value!='three']", List.class);
        Map actual = (Map) list.get(0);
        assertEquals(3, actual.size());

        Map newMap = simpleExpress("#map.?[key != 4].?[value!='three']", Map.class);
        assertEquals(2, newMap.size());
    }

    private List<Integer> getIntegers() {
        return Arrays.asList(1, 3, 5, 7);
    }

    private Map<Integer, String> getIntegerStringMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        return map;
    }

    private <T> T simpleExpress(String expression, Class<T> t) {
        return expressionParser.parseExpression(expression).getValue(context, t);
    }
}
