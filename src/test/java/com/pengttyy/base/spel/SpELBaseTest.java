package com.pengttyy.base.spel;

import org.junit.Before;
import org.junit.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import static org.junit.Assert.*;

/**
 * SpEL基本的表达式
 * Created by KaiPeng on 2016/7/5.
 */
public class SpELBaseTest {

    private ExpressionParser expressionParser;
    private EvaluationContext context;

    @Before
    public void setUp() throws Exception {
        //解析器
        expressionParser = new SpelExpressionParser();
        //上下文
        context = new StandardEvaluationContext();
    }

    @Test
    public void helloWorld() throws Exception {
        //表达式
        Expression expression = expressionParser.parseExpression("('Hello'+' World').concat(#end)");
        //设置变量
        context.setVariable("end", "!!!!");
        String value = expression.getValue(context, String.class);
        assertEquals("Hello World!!!!", value);
    }

    /**
     * 数据类型中的null,其它类型基本与java相同
     *
     * @throws Exception
     */
    @Test
    public void given_null_then_null() throws Exception {
        Expression expression = expressionParser.parseExpression("null");
        Object value = expression.getValue(context);
        assertNull(value);
    }

    /**
     * 包含区间上限和下限
     *
     * @throws Exception
     */
    @Test
    public void given_between_then_true() throws Exception {
        Boolean aBoolean = simpleExpress("1 between {1,2}", boolean.class);
        assertTrue(aBoolean);

        Boolean aBoolean1 = simpleExpress("2 between {1,2}", boolean.class);
        assertTrue(aBoolean1);

        Boolean aBoolean2 = simpleExpress("3 between {1,2}", boolean.class);
        assertFalse(aBoolean2);

    }

    /**
     * 三目运算符及Elivis运算符
     *
     * @throws Exception
     */
    @Test
    public void given_ternary_then_value() throws Exception {
        String express = simpleExpress("2>1?'true':'false'", String.class);
        assertEquals("true", express);

        //Elivis运算符：三目运算符的简化版
        String express1 = simpleExpress("'不为null'?:'为null时显示'", String.class);
        assertEquals("不为null", express1);
    }

    @Test
    public void given_regex_then_matches() throws Exception {
        Boolean aBoolean = simpleExpress("'2224' matches '\\d{4}'", boolean.class);
        assertTrue(aBoolean);
    }

    private <T> T simpleExpress(String expression, Class<T> t) {
        return expressionParser.parseExpression(expression).getValue(t);
    }
}
