package com.pengttyy.base.aop.aspectj;

public interface IHelloAspectJ {
    void sayHelloBefore();

    String sayHelloAfterReturning();

    void sayHelloAfterThrowing(String myParam) throws Exception;
}
