package com.pengttyy.base.aop.aspectj;

import org.aspectj.lang.annotation.*;

/**
 * Created by KaiPeng on 2016-07-10,0010.
 */
@Aspect
public class HelloWorldAspect2 {

    @DeclareParents(value = "com.pengttyy.base.aop.aspectj.IHelloAspectJ+", defaultImpl = IntroductionAspectjService.class)
    private IIntroductionAspectj introductionAspectj;

    @Pointcut("execution(* com.pengttyy.base.aop.aspectj.HelloServiceAspectJ.sayHelloBefore())")
    public void beforePointcut() {
    }

    ///@Before("beforePointcut()")
    @Before("execution(* com.pengttyy.base.aop.aspectj.HelloServiceAspectJ.sayHelloBefore())")
    public void beforeAdvice() {
        System.out.println("beforeAdvice");
    }

    @AfterReturning(value = "execution(* com.pengttyy.base.aop.aspectj.HelloServiceAspectJ.sayHelloAfterReturning())", returning = "result")
    public void afterReturningAdvice(String result) {
        System.out.println("这是方法返回值：" + result);
    }

    @AfterThrowing(value = "execution(* com.pengttyy.base.aop.aspectj.HelloServiceAspectJ.sayHelloAfterThrowing(..)) and args(param)", throwing = "e", argNames = "param,e")
    public void afterThrowingAdvice(String param, Exception e) {
        System.out.println("afterThrowingAdvice===========" + e.getMessage());
        System.out.println("afterThrowingAdvice这是参数===========" + param);
    }


}
