package com.pengttyy.base.aop.aspectj;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016-07-10,0010.
 */
public class HelloServiceAspectJTest extends SpringUnitTestBase {
    @Test
    public void aop_hello_world_before() throws Exception {
        IHelloAspectJ bean = this.context.getBean("aop_hello_world_aspectj", IHelloAspectJ.class);
        bean.sayHelloBefore();
    }

    @Test
    public void aop_hello_world_after_returning() throws Exception {
        IHelloAspectJ bean = this.context.getBean("aop_hello_world_aspectj", IHelloAspectJ.class);
        bean.sayHelloAfterReturning();
    }

    @Test(expected = Exception.class)
    public void aop_hello_world_after_throwing() throws Exception {
        IHelloAspectJ bean = this.context.getBean("aop_hello_world_aspectj", IHelloAspectJ.class);
        bean.sayHelloAfterThrowing("myParam");
    }

    @Test
    public void aop_hello_world_import() throws Exception {
        IIntroductionAspectj bean = this.context.getBean("aop_hello_world_aspectj", IIntroductionAspectj.class);
        bean.induct();
//        IHello bean1 = (IHello) bean;
//        bean1.sayHello();
    }
}
