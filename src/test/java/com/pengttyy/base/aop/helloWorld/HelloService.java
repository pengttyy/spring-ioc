package com.pengttyy.base.aop.helloWorld;

/**
 * Created by KaiPeng on 2016/7/9.
 */
public class HelloService implements IHello {
    @Override
    public void sayHello() {
        System.out.println("Hello World!");
    }

    @Override
    public void sayHello(String param) {
        System.out.println("hello world!" + param);
    }

    @Override
    public void sayHello2() {
        System.out.println("hello world2");
    }

    @Override
    public boolean sayHelloReturn() {
        System.out.println("hello world sayHelloReturn");
        return false;
    }

    @Override
    public boolean sayHelloafterThrowing() {
        System.out.println("后置异常方法");
        throw new RuntimeException("测试after-throwing");
    }

    @Override
    public void sayHelloAround(String pk) {
        System.out.println("hello world-around" + pk);
    }

}
