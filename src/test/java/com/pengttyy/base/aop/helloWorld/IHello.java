package com.pengttyy.base.aop.helloWorld;

public interface IHello {
    public void sayHello();

    public void sayHello(String param);
    public void sayHello2();

    boolean sayHelloReturn();

    boolean sayHelloafterThrowing();

    void sayHelloAround(String pk);
}
