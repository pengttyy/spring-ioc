package com.pengttyy.base.aop.helloWorld;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * Created by KaiPeng on 2016/7/9.
 */
public class HelloWorldAspect {
    public void beforeAdvice() {
        System.out.println("前置通知");
    }

    public void afterAdivice() {
        System.out.println("后置通知");
    }

    public void beforeAdviceArg(String xxxx) {
        System.out.println("有参数的前置通知param:" + xxxx);
    }

    public void afterReturnAdvice(boolean xxxx) {
        System.out.println("有返回值的后置返回通知return:" + xxxx);
    }

    public void afterThrowAdvice(Exception e) {
        System.out.println("后置异常通知：exception:" + e.getMessage());
    }

    public Object aroundAdvice(ProceedingJoinPoint pip, String param) {
        try {
            System.out.println("around_before");
            Object obj = pip.proceed(new Object[]{param});
            System.out.println("around_after_runing");
            return obj;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            System.out.println("around_after_throws");
            return null;
        } finally {
            System.out.println("around_after_finally");
        }
    }
}
