package com.pengttyy.base.aop.order;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016-07-10,0010.
 */
public class OrderTest extends SpringUnitTestBase {
    @Test
    public void aop_order() throws Exception {
        IHelloOrder bean = this.context.getBean("orderAOP", IHelloOrder.class);
        bean.sayHelloWorld();
    }
}
