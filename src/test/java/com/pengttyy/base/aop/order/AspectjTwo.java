package com.pengttyy.base.aop.order;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

/**
 * Created by KaiPeng on 2016-07-10,0010.
 */
@Aspect
@Order(1)
public class AspectjTwo {

    @Before("execution(* com.pengttyy.base.aop.order.HelloOrderService.sayHelloWorld())")
    public void beforeTwo() {
        System.out.println(this.getClass().getName() + ".beforeTwo");
    }

    @Before("execution(* com.pengttyy.base.aop.order.HelloOrderService.sayHelloWorld())")
    public void beforeOne() {
        System.out.println(this.getClass().getName() + ".beforeOne");
    }

    @After("execution(* com.pengttyy.base.aop.order.HelloOrderService.sayHelloWorld())")
    public void afterTwo() {
        System.out.println(this.getClass().getName() + ".afterTwo");
    }

    @After("execution(* com.pengttyy.base.aop.order.HelloOrderService.sayHelloWorld())")
    public void afterOne() {
        System.out.println(this.getClass().getName() + ".afterOne");
    }
}
