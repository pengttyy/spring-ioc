package com.pengttyy.base.resource;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.Resource;

/**
 * Resource注入测试类
 * Created by wseng9 on 2016/7/4.
 */
public class ResourceInjectTest extends SpringUnitTestBase {
    @Test
    public void given_url_then_resource() throws Exception {
        ResourceInject resourceInject = this.getBean("resourceInject", ResourceInject.class);
        Resource file = resourceInject.getFile();
        Assert.assertTrue(file.exists());

    }
}
