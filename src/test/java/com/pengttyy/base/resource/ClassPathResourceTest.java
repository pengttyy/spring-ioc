package com.pengttyy.base.resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import static org.junit.Assert.assertTrue;

/**
 * Resource通配符路径
 * Created by KaiPeng on 2016/7/4.
 */
public class ClassPathResourceTest {

    private PathMatchingResourcePatternResolver resolver;

    @Before
    public void setUp() throws Exception {
        this.resolver = new PathMatchingResourcePatternResolver();
    }

    @Test
    public void given_classpath_then_one_resource() throws Exception {
        Resource[] resources = resolver.getResources("classpath:com/pengttyy/base/resource/test.txt");
        assertTrue(resources.length == 1);
    }

    @Test
    public void given_classpath_then_many_resource() throws Exception {
        Resource[] resources = resolver.getResources("classpath:com/pengttyy/base/resource/*.xml");
        assertTrue(resources.length != 1);
    }

    /**
     * 取得所有classpath下面的文件，除了jar
     *
     * @throws Exception
     */
    @Test
    public void given_classpaths_then_many_resource() throws Exception {
        Resource[] resources = resolver.getResources("classpath*:**/*.class");
        assertTrue(resources.length > 1);
    }

    /**
     * 在通过前缀“classpath*”加载通配符路径时，必须包含一个根目录才能保证加载的资源是所有的，而不是部分。
     * <p>
     * 原因：“ClassLoader”的“getResources(String name)”方法的限制，对于name为“”的情况将只返回文件系统的类路径，不会包换jar包根路径。
     *
     * @throws Exception
     */
    @Test
    public void given_classpaths_jar_then_many_resource() throws Exception {
        Resource[] resources = resolver.getResources("classpath*:LICENSE.txt");
        assertTrue(resources.length > 1);
    }
}
