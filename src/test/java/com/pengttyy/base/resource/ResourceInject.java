package com.pengttyy.base.resource;

import org.springframework.core.io.Resource;

/**
 * 注入resource bean
 * Created by wseng9 on 2016/7/4.
 */
class ResourceInject {
    private Resource file;

    public Resource getFile() {
        return file;
    }

    public void setFile(Resource file) {
        this.file = file;
    }
}
