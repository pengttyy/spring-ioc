package com.pengttyy.base.orm.hibernate;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

/**
 * Created by wseng9 on 2016/7/13.
 */
public class UserDaoImpl extends HibernateDaoSupport implements IuserDao {

    @Override
    public void save(User user) {
        this.getHibernateTemplate().save(user);
    }

    @Override
    public void update(User user) {

    }

    @Override
    public User get(int id) {
        return null;
    }

    @Override
    public void delete(int id) {

    }
}
