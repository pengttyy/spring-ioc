package com.pengttyy.base.beanName.beanDefindNotID;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by KaiPeng on 2016/6/9.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        NotIDTest.class,
        NotIDTwoBeanDefindTest.class
})
public class NotIDSuite {
}
