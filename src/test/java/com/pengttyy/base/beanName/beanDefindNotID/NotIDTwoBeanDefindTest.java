package com.pengttyy.base.beanName.beanDefindNotID;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;

/**
 * Created by KaiPeng on 2016/6/9.
 */
public class NotIDTwoBeanDefindTest extends SpringUnitTestBase {
    public NotIDTwoBeanDefindTest() {
        super("com/pengttyy/base/beanName/beanDefindNotID/twoNotId.xml");
    }

    @Test(expected = NoUniqueBeanDefinitionException.class)
    public void testTwoNoId() throws NoUniqueBeanDefinitionException {
        String bean = this.getBean(String.class);
    }

}
