package com.pengttyy.base.beanName;

import com.pengttyy.base.beanName.beanDefindID.IDSuite;
import com.pengttyy.base.beanName.beanDefindIDAndName.DefindIDAndName;
import com.pengttyy.base.beanName.beanDefindName.DefindNameTest;
import com.pengttyy.base.beanName.beanDefindName.DefindTwoNameTest;
import com.pengttyy.base.beanName.beanDefindNotID.NotIDSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by KaiPeng on 2016/6/9.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        IDSuite.class,
        NotIDSuite.class,
        DefindIDAndName.class,
        DefindNameTest.class,
        DefindTwoNameTest.class
})
public class BeanNameSuite {
}
