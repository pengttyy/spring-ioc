package com.pengttyy.base.beanName.beanDefindID;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016/6/9.
 */
public class DefindIDTest extends SpringUnitTestBase {
    public DefindIDTest() {
        super("com/pengttyy/base/beanName/beanDefindID/defindId.xml");
    }

    @Test
    public void testDefindId() throws Exception {
        String myBean = this.context.getBean("myBean", String.class);
        Assert.assertEquals("hello spring", myBean);
    }
}
