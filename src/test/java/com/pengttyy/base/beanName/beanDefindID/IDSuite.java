package com.pengttyy.base.beanName.beanDefindID;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by KaiPeng on 2016/6/9.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        DefindIDTest.class,
        DefindTwoIDTest.class
})
public class IDSuite {
}
