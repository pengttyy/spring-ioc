package com.pengttyy.base.beanName.beanDefindID;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016/6/9.
 * xml中配置bean,不能有重复
 */
public class DefindTwoIDTest extends SpringUnitTestBase {

    @Test//(expected = BeanDefinitionParsingException.class)
    public void testDefindTwoIDTest() throws Exception {
        String myBean = this.context.getBean("myBean", String.class);
        System.out.println(myBean);
    }
}
