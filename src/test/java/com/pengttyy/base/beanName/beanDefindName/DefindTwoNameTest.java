package com.pengttyy.base.beanName.beanDefindName;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 根据name获取bean不能指定同名的name
 * Created by KaiPeng on 2016/6/9.
 */
public class DefindTwoNameTest extends SpringUnitTestBase {
    @Test
    @Ignore
    public void testDefindName() throws Exception {
        String myBeanName = this.context.getBean("myBeanName", String.class);
        assertEquals("hello spring", myBeanName);
    }
}
