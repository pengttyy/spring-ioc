package com.pengttyy.base.beanName.beanDefindName;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 根据name获取
 * Created by KaiPeng on 2016/6/9.
 */
public class DefindNameTest extends SpringUnitTestBase {
    @Test
    public void testDefindName() throws Exception {
        String myBeanName = this.context.getBean("myBeanName", String.class);
        assertEquals("hello spring", myBeanName);
    }
}
