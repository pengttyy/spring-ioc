package com.pengttyy.base.transaction;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * 使用PlatformTransactionManager来管理事务
 * Created by KaiPeng on 2016-07-16,0016.
 */
public class PlatformTransactionManagerTest extends SpringUnitTestBase {
    private PlatformTransactionManager transactionManager;
    private DefaultTransactionDefinition definition;
    private JdbcTemplate template;

    @Override
    public void before() throws Exception {
        super.before();
        this.transactionManager = this.getBean(PlatformTransactionManager.class);
        definition = new DefaultTransactionDefinition();
        definition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        definition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        template = this.getBean(JdbcTemplate.class);
    }

    @Test
    public void user_PlatformTransactionManager_nomal_commit() {
        TransactionStatus transaction = transactionManager.getTransaction(definition);
        template.update("INSERT INTO USERS(name) VALUES (?)", "PlatformTransactionManagerTest_commit");

        transactionManager.commit(transaction);

        Integer count = this.template.queryForObject("SELECT count(1) FROM USERS WHERE NAME=?", int.class, "PlatformTransactionManagerTest_commit");
        Assert.assertTrue(count > 0);
    }

    @Test
    public void user_PlatformTransactionManager_rollback() throws Exception {
        TransactionStatus transaction = transactionManager.getTransaction(definition);
        template.update("INSERT INTO USERS(name) VALUES (?)", "PlatformTransactionManagerTest_commit");

        transactionManager.rollback(transaction);

        Integer count = this.template.queryForObject("SELECT count(1) FROM USERS WHERE NAME=?", int.class, "PlatformTransactionManagerTest_commit");
        Assert.assertFalse(count > 0);
    }

    @Override
    public void after() throws Exception {
        TransactionStatus transaction = transactionManager.getTransaction(definition);
        this.template.update("DELETE FROM USERS");
        this.transactionManager.commit(transaction);
        super.after();
    }
}
