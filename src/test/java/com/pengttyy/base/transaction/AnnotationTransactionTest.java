package com.pengttyy.base.transaction;

import com.pengttyy.base.SpringUnitTestBase;
import com.pengttyy.base.orm.hibernate.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;

import java.math.BigInteger;

/**
 * Created by KaiPeng on 2016-07-17,0017.
 */
public class AnnotationTransactionTest extends SpringUnitTestBase {

    private HibernateTemplate template;

    @Override
    public void before() throws Exception {
        super.before();
        template = this.context.getBean(HibernateTemplate.class);
    }

    @Test
    public void use_transaction_declare() throws Exception {
        IuserService userService = this.getBean(IuserService.class);
        User user = new User();
        user.setName("use_transaction_declare");
        userService.saveUser(user);
        Assert.assertTrue(count() == 1);
    }

    private Integer count() {
        return this.template.execute(new HibernateCallback<Integer>() {
            @Override
            public Integer doInHibernate(Session session) throws HibernateException {
                Object o = session.createSQLQuery("select count(1) from Users").uniqueResult();
                Integer count1 = ((BigInteger) o).intValue();
                return count1;
            }
        });
    }

    @Test
    public void use_transaction_declare_throw() throws Exception {
        IuserService userService = this.getBean(IuserService.class);
        User user = new User();
        user.setName("use_transaction_declare");
        try {
            userService.saveUserThrow(user);
        } catch (Exception e) {
            //异常不是关注点
        }
        Assert.assertTrue(count() == 0);
    }


    @Override
    public void after() throws Exception {
        HibernateTemplate template = this.context.getBean(HibernateTemplate.class);
        template.executeWithNativeSession(new HibernateCallback<Object>() {
            @Override
            public Object doInHibernate(Session session) throws HibernateException {
                session.createSQLQuery("DELETE FROM USERS").executeUpdate();
                return null;
            }
        });
        super.after();
    }
}
