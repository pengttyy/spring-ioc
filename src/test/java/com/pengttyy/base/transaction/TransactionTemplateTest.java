package com.pengttyy.base.transaction;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Created by KaiPeng on 2016-07-16,0016.
 */
public class TransactionTemplateTest extends SpringUnitTestBase {
    private TransactionTemplate transactiontemplate;
    private JdbcTemplate template;

    @Override
    public void before() throws Exception {
        super.before();
        this.transactiontemplate = this.getBean(TransactionTemplate.class);
        this.template = this.getBean(JdbcTemplate.class);
    }

    @Test
    public void user_TransactionTemplate_commit() throws Exception {
        this.transactiontemplate.execute(new TransactionCallback<Integer>() {//TransactionTemplate自动管理事务
            @Override
            public Integer doInTransaction(TransactionStatus transactionStatus) {
                return template.update("INSERT INTO USERS(name) VALUES (?)", "user_TransactionTemplate_commit");
            }
        });
    }

    @Override
    public void after() throws Exception {
        this.template.execute("DELETE FROM USERS");
        super.after();
    }
}
