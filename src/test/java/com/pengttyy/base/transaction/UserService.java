package com.pengttyy.base.transaction;

import com.pengttyy.base.orm.hibernate.IuserDao;
import com.pengttyy.base.orm.hibernate.User;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by KaiPeng on 2016-07-17,0017.
 */
public class UserService implements IuserService {
    private IuserDao userDao;

    public void setUserDao(IuserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public void saveUser(User user) {
        this.userDao.save(user);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public void saveUserThrow(User user) throws Exception {
        this.userDao.save(user);
        throw new RuntimeException("故意异常");
    }
}
