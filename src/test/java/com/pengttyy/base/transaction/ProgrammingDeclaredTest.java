package com.pengttyy.base.transaction;

import com.pengttyy.base.SpringUnitTestBase;
import com.pengttyy.base.orm.hibernate.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.HibernateTemplate;

/**
 * Created by KaiPeng on 2016-07-17,0017.
 */
public class ProgrammingDeclaredTest extends SpringUnitTestBase {
    @Test
    public void use_transaction_declare() throws Exception {
        IuserService userService = this.getBean(IuserService.class);
        User user = new User();
        user.setName("use_transaction_declare");
        userService.saveUser(user);
    }

    @Override
    public void after() throws Exception {
        HibernateTemplate template = this.context.getBean(HibernateTemplate.class);
        template.executeWithNativeSession(new HibernateCallback<Object>() {
            @Override
            public Object doInHibernate(Session session) throws HibernateException {
                session.createSQLQuery("DELETE FROM USERS").executeUpdate();
                return null;
            }
        });
        super.after();
    }
}
