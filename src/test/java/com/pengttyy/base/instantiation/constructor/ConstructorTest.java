package com.pengttyy.base.instantiation.constructor;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016/6/9.
 */
public class ConstructorTest extends SpringUnitTestBase {
    @Test
    public void use_then_index() throws Exception {
        Person person = this.getBean("use_bean_index", Person.class);
    }

    @Test
    public void use_then_type() throws Exception {
        Person person = this.getBean("use_bean_type", Person.class);
    }

    @Test
    public void use_then_argName() throws Exception {
        Person person = this.getBean("use_bean_argName", Person.class);
        System.out.println(person);
    }

    @Test
    public void use_then_mixture() throws Exception {
        Person person = this.getBean("use_then_mixture", Person.class);
        System.out.println(person);
    }

    @Test
    public void use_then_staticFactory() throws Exception {
        Person person = this.getBean("use_then_staticFactory", Person.class);
        System.out.println(person);
    }

    @Test
    public void use_then_factory() throws Exception {
        Person person = this.getBean("use_then_factory", Person.class);

    }
}
