package com.pengttyy.base.instantiation.setter;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;

/**
 * Created by KaiPeng on 2016/6/10.
 */
public class SetterTest extends SpringUnitTestBase {
    @Test
    public void use_then_setter() throws Exception {
        PersonStub bean = this.getBean("setterID", PersonStub.class);
        System.out.println(bean);
    }
}
