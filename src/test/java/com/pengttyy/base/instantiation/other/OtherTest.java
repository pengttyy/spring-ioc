package com.pengttyy.base.instantiation.other;

import com.pengttyy.base.SpringUnitTestBase;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by KaiPeng on 2016/6/11.
 */
public class OtherTest extends SpringUnitTestBase {
    @Test
    public void user_when_ref_bean_constructor() throws Exception {
        AssertRefBean("user_when_ref_bean_constructor");
    }

    private void AssertRefBean(String beanName) {
        Other other = this.getBean(beanName, Other.class);
        String refBean = other.getRefBean();
        assertEquals("refString", refBean);
    }

    @Test
    public void use_when_ref_bean_setter() throws Exception {
        AssertRefBean("use_when_ref_bean_setter");
    }

    @Test
    public void use_when_ref_local_bean() throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"com/pengttyy/base/instantiation/other/importConfig.xml"}, this.context);
        Other other = context.getBean("use_when_ref_local_bean", Other.class);
        String refBean = other.getRefBean();
        assertEquals("refString current", refBean);
    }

    @Test
    public void use_when_ref_parent_bean() throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"com/pengttyy/base/instantiation/other/importConfig.xml"}, this.context);
        Other other = context.getBean("use_when_ref_parent_bean", Other.class);
        String refBean = other.getRefBean();
        assertEquals("refString", refBean);
    }

    /**
     * 空值
     *
     * @throws Exception
     */
    @Test
    public void use_when_null() throws Exception {
        Other other = this.getBean("use_when_null", Other.class);
        String refBean = other.getRefBean();
        assertNull(refBean);
    }

    @Test
    public void use_when_map_navigation() throws Exception {
        MapNavigation navigation = this.getBean("use_when_map_navigation", MapNavigation.class);
        String arrTwoStr = navigation.getArrStr()[2];
        assertEquals("two", arrTwoStr);

        String listStr = navigation.getList().get(2);
        assertEquals("two", listStr);

        String mapVal = navigation.getMap().get("key");
        assertEquals("two", mapVal);

        String propVal = navigation.getProperties().getProperty("key");
        assertEquals("two", propVal);
    }
}
