package com.pengttyy.base.instantiation.other;

/**
 * Created by wseng9 on 2016/6/23.
 */
public class ResourceObj {
    public void init() {
        System.out.println(this.getClass().getName() + "init....");
    }

    public void destroy() {
        System.out.println(this.getClass().getName() + "destroy....");
    }

    void execute() {
        System.out.println("执行一些操作");
    }
}
