package com.pengttyy.base.instantiation.other.lookup;

public class Singletion {
    private Prototype prototype;

    public Prototype getPrototype() {
        return this.prototype;
    }

    public void setPrototype(Prototype prototype) {
        this.prototype = prototype;
    }

    public String targetReplaceMethod() {
        System.out.println("未替换的方法");
        return "targetReplaceMethod";
    }
}
